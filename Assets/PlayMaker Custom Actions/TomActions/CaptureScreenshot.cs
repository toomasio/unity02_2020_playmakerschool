// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Camera)]
	[Tooltip("Takes the current view and resolution of the game camera and converts/stores it to a .png")]
	public class CaptureScreenshot : FsmStateAction
	{
		[Tooltip("The file path of the saved screenshot relative to the Assets folder")]
		public FsmString filePath;

		[Tooltip("The file name of the saved screenshot")]
		public FsmString fileName;

		[Tooltip("The file path of the saved screenshot")]
		public FsmInt resolutionIncrease;

		public override void Reset()
		{
			filePath = "Assets/YOURFOLDERS/";
			resolutionIncrease = null;
		}

		public override void OnEnter()
		{
			var screenshotFullPath = Application.dataPath + "/" + filePath.Value + fileName.Value + ".png";
			ScreenCapture.CaptureScreenshot(screenshotFullPath, resolutionIncrease.Value);

			Finish();
		}
	}
}

