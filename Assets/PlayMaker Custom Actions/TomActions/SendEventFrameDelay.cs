// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.StateMachine)]
    [ActionTarget(typeof(PlayMakerFSM), "eventTarget")]
    [ActionTarget(typeof(GameObject), "eventTarget")]
	[Tooltip("Sends an Event after a frame delay. NOTE: To send events between FSMs they must be marked as Global in the Events Browser.")]
	public class SendEventFrameDelay : FsmStateAction
	{
		public enum UpdateType { OnUpdate, OnFixedUpdate, OnLateUpdate }

		[Tooltip("Where to send the event.")]
		public FsmEventTarget eventTarget;
		
		[RequiredField]
		[Tooltip("The event to send. NOTE: Events must be marked Global to send between FSMs.")]
		public FsmEvent sendEvent;

		[Tooltip("Amount of frames to wait to send event.")]
		public FsmInt frameDelay;

		[Tooltip("The update type when calculating the frames.")]
		public UpdateType updateType;

		private int curFrames;

		public override void Reset()
		{
			eventTarget = null;
			sendEvent = null;
			frameDelay = 1;
			curFrames = 0;
			updateType = default;
		}

		public override void OnPreprocess()
		{
			if (updateType == UpdateType.OnFixedUpdate)
				Fsm.HandleFixedUpdate = true;
			else if (updateType == UpdateType.OnLateUpdate)
				Fsm.HandleLateUpdate = true;
		}

		public override void OnUpdate()
		{
			if (updateType != UpdateType.OnUpdate)
				return;

			DoDelay();
		}

		public override void OnFixedUpdate()
		{
			if (updateType != UpdateType.OnFixedUpdate)
				return;

			DoDelay();
		}

		public override void OnLateUpdate()
		{
			if (updateType != UpdateType.OnLateUpdate)
				return;

			DoDelay();
		}

		void DoDelay()
		{
			curFrames++;
			if (curFrames >= frameDelay.Value)
			{
				Fsm.Event(eventTarget, sendEvent);
				Finish();
			}
		}

#if UNITY_EDITOR

	    public override string AutoName()
	    {
	        return "SendEvent : " + (eventTarget.target != FsmEventTarget.EventTarget.Self ? " " + eventTarget.target + " ": "")
	                              + (sendEvent != null ? sendEvent.Name : "None");
	    }
#endif
	}
}