﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonExample : MonoBehaviour
{
    private static SingletonExample instance;

    private GameObject player;
    private Vector3 lastCheckPoint;
    public static SingletonExample Instance
    {
        get
        {
            if (instance)
                return instance;
            else
                return new GameObject("SingletonName").AddComponent<SingletonExample>();
        }
    }

    private void Awake()
    {
        //duplicate check
        if (!instance)
        {
            //initialization stuff here...
            instance = this;
        }   
        else
            Destroy(gameObject);
    }

    public void SetPlayer(GameObject _player)
    {
        player = _player;
    }

    public void SetLastCheckPoint(Vector3 _position)
    {
        lastCheckPoint = _position;
    }

    public void WarpToLastCheckpoint()
    {
        player.transform.position = lastCheckPoint;
    }

}
