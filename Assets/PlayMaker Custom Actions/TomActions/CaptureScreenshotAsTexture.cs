// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using System.Collections;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Camera)]
	[Tooltip("Takes the current view of the game camera and converts it to a Texture. This action takes 1 frame to process")]
	public class CaptureScreenshotAsTexture : FsmStateAction
	{
		[Tooltip("The file path of the saved screenshot")]
		[UIHint(UIHint.Variable), RequiredField]
		public FsmTexture texture;

		[Tooltip("The file path of the saved screenshot")]
		public FsmInt resolutionIncrease;

		[Tooltip("The event to send after the texture is created")]
		public FsmEvent onFinished;

		private Coroutine routine;

		public override void Reset()
		{
			texture = null;
			resolutionIncrease = null;
		}

		public override void OnEnter()
		{
			routine = StartCoroutine(StartTakeScreenShot());
		}

		IEnumerator StartTakeScreenShot()
		{
			yield return new WaitForEndOfFrame();
			TakeScreenShot();
		}

		void TakeScreenShot()
		{
			texture.Value = ScreenCapture.CaptureScreenshotAsTexture(resolutionIncrease.Value);
			Fsm.Event(onFinished);
		}

		public override void OnExit()
		{
			StopCoroutine(routine);
		}
	}
}

