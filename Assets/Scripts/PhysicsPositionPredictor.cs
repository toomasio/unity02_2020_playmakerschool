﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsPositionPredictor : MonoBehaviour
{
    public enum PredictionType { Positions, Force, Time, Angle }
    [SerializeField] private PredictionType predictionType;
    [SerializeField] private LineRenderer line;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float launchForce;
    [SerializeField] private float launchAngle;
    [SerializeField] private float launchAngle2;
    [SerializeField] private float launchTime;
    [SerializeField] private Transform targetPos;
    [SerializeField] private int positionPoints;
    [SerializeField] private float timeStep = 0.01f;

    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (predictionType == PredictionType.Positions)
        {
            positionPoints = Mathf.Clamp(positionPoints, 2, int.MaxValue);
            var points = GetTrajectoryPredictionPoints(transform.position, transform.forward * launchForce,
                rb.drag, positionPoints, timeStep);
            line.positionCount = positionPoints;
            line.SetPositions(points);
        }
        

        if (Input.GetButtonDown("Fire1"))
        {
            var spawn = Instantiate(rb, transform.position, Quaternion.identity);
            spawn.velocity = transform.forward * launchForce;
        }
    }

    Vector3[] GetTrajectoryPredictionPoints(Vector3 _startPos, Vector3 _velocity, float _drag, int _steps, float _time)
    {
        var pos = _startPos;
        var vel = _velocity;
        var acc = Physics.gravity;
        var dt = Time.fixedDeltaTime / Physics.defaultSolverVelocityIterations;
        var drag = 1 - (_drag * dt);
        var points = new Vector3[_steps];
        points[0] = _startPos;
        for (int i = 1; i < _steps; i++)
        {
            float t = 0;
            while (t < _time)
            {
                vel += acc * dt;
                pos += vel * dt;
                pos *= drag;
                t += dt;
            } 
            points[i] = pos;
        }
        return points;
    }

    
}
