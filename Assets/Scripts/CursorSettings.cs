﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorSettings : MonoBehaviour
{
    [SerializeField] private CursorLockMode lockMode;
    [SerializeField] private bool visible = true;

    private void Start()
    {
        Cursor.lockState = lockMode;
        Cursor.visible = visible;
    }
}
