﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonTest : MonoBehaviour
{
    public SingletonExample example;
    private void Start()
    {
        example = SingletonExample.Instance;
    }

}
